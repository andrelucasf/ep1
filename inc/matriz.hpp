#ifndef MATRIZ_HPP
#define MATRIZ_HPP
#include <string>

#define GNUM 100

using namespace std;

class Matriz{
private:
    char celula[GNUM][GNUM];
    string nome;
    int repeticoes;
    int tamanho;

public:
    Matriz();
    Matriz(string nome, int tamanho, int repeticoes);
    ~Matriz();

    char getCelula(int linha, int coluna);
    void setCelula(char celula, int linha, int coluna);

    string getNome();
    void setNome(string nome);

    int getTamanho();
    void setTamanho(int tamanho);

    int getRepeticoes();
    void setRepeticoes(int repeticoes);

    void montaAleatoria();
		void juntaMatriz();
		void adicionaElemento(char letra, int x, int y);

    int celulasVivas(int linha, int coluna);
    int celulasVivas();

};

#endif
