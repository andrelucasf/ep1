#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include "matriz.hpp"
#include "block.hpp"
#include "blinker.hpp"
#include "glider.hpp"
#include "gliderGun.hpp"

Matriz escolheMatriz(char letra){

	Block block;
	Blinker blinker;
	Glider glider;
	gliderGun gun;

	if (letra == 'k') {
		return block;
	}else if (letra == 'b') {
		return blinker;
	}else if (letra == 'g') {
		return glider;
	}else{
		return gun;
	}
}

int main(int argc, char ** argv) {
	Matriz habitat, memoria;
	int linha, coluna, repeticoes, contador = 0, vivas, tamanho, celulasVivas = 1;
	char resposta;

	// Menu:
	cout << endl << "Este programa é uma implementação C ++ do jogo de vida de André Lucas."
	<< endl << "Com isso, você pode simular como \"células\" interagem uns com os outros." << endl
	<< endl << "As regras do \"Game of Life\"são as seguintes:" << endl
	<< endl << "1. Qualquer célula viva com menos de dois vizinhos vivos morre de solidão."
	<< endl << "2. Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo estado para a próxima geração."
	<< endl << "3. Qualquer célula viva com mais de três vizinhos vivos morre de superlotação."
	<< endl << "4. Qualquer célula morta com exatamente três vizinhos vivos se torna uma célula viva" << endl;
	cout << "DIGITE:\n K para Block\n B para Blinker\n G para Glider\n" << endl;
	cout << " U para Glider Gun\n " << endl;

	cin >> resposta;
	cout << endl;

	if (resposta < 97) {
		resposta = resposta + 32;
	}

	memoria = habitat;
	tamanho = habitat.getTamanho();

	cout << endl;
	cout << "Você escolheu: " <<  habitat.getNome() << endl;
	cout << " - É uma matriz "<< habitat.getTamanho() << "X" << habitat.getTamanho() << endl;
	cout << " - Possui " << habitat.getRepeticoes() << " repeticoes" << endl;

	cout << "\nDeseja prosseguir, a quantidade de repeticoes das suas celulas é 40:(S) ";
	cin >> resposta;
	cout << endl;

	while (resposta != 's' && resposta != 'S'){
		cout << "\n\nERRO: ESCREVA 'S' PARA SIM";
		cin >> resposta;
		cout << endl;
	}

	if (resposta == 's' || resposta == 'S') {
		cout << "REPETICÕES: ";
		cin >> repeticoes;
		habitat.setRepeticoes(repeticoes);
	}

	sleep(1);

	}

	while (contador < habitat.getRepeticoes() && celulasVivas > 0) {
		contador++;

			for(coluna = 0; coluna < tamanho; coluna++){

				cout << habitat.getCelula(linha, coluna) << "  ";

				if (linha > 0 && linha < (tamanho-1) && coluna > 0 && coluna < (tamanho-1)) {
					vivas = memoria.celulasVivas(linha,coluna);

					// REGRAS:
					if (habitat.getCelula(linha,coluna) == '+' && (vivas < 2 || vivas > 3)) {
						habitat.setCelula(' ',linha,coluna);
					}else if(habitat.getCelula(linha,coluna) == ' ' && vivas == 3){
						habitat.setCelula('+',linha,coluna);
					}else if(habitat.getCelula(linha,coluna) == '+' && (vivas == 2 || vivas == 3)){
						habitat.setCelula('+',linha,coluna);
					}
				}
			}
			cout << endl;
		}

		celulasVivas = memoria.celulasVivas();

		if (celulasVivas == 0) {
			cout << endl << "Suas celulas morreram." << endl;
		}

		memoria = habitat;
		usleep(250000);

return 0;
}
