#include <iostream>
#include "block.hpp"

Block::Block(){

  setCelula('+', 10, 10);
  setCelula('+', 10, 11);
  setCelula('+', 11, 10);
  setCelula('+', 11, 11);

  setNome("Block");
  setRepeticoes(5);
  setTamanho(20);
}

Block::~Block(){}
