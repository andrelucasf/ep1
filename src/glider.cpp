#include <iostream>
#include "glider.hpp"

Glider::Glider(){

  setCelula('+', 0, 1);
  setCelula('+', 1, 2);
  setCelula('+', 2, 0);
  setCelula('+', 2, 1);
  setCelula('+', 2, 2);

  setNome("Glider");
  setRepeticoes(60);
  setTamanho(20);
}

Glider::~Glider(){}
