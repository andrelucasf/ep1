#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "matriz.hpp"
#include "glider.hpp"
#include "block.hpp"
#include "blinker.hpp"

Matriz::Matriz(){
    int lin, col;
    nome = "Células mortas";
    repeticoes = 100;
    tamanho = 20;

    for (lin = 0; lin < 100; lin++) {
        for (col = 0; col < 100; col++) {
                celula[lin][col] = ' ';
        }
    }
}

Matriz::Matriz(string nome, int tamanho, int repeticoes){
  int lin, col;

  this->nome = nome;
  this->tamanho = tamanho;
  this->repeticoes = repeticoes;

  for (lin = 0; lin < 100; lin++) {
      for (col = 0; col < 100; col++) {
              celula[lin][col] = ' ';
      }
  }
}
Matriz::~Matriz(){
}

char Matriz::getCelula(int lin, int col){
    return celula[lin][col];
}

void Matriz::setCelula(char celula, int lin, int col){
    this->celula[lin][col] = celula;
}
string Matriz::getNome(){
    return nome;
}

void Matriz::setNome(string nome){
    this->nome = nome;
}

int Matriz::getRepeticoes(){
    return repeticoes;
}

void Matriz::setRepeticoes(int repeticoes){
    this->repeticoes = repeticoes;
}

int Matriz::getTamanho(){
    return tamanho;
}

void Matriz::setTamanho(int tamanho){
    this->tamanho = tamanho;
}

int Matriz::celulasVivas(int lin, int col){

  int vivas = 0;
  if (celula[lin-1][col-1] == '+'){
  vivas++;
  }
  if (celula[lin-1][col] == '+'){
  vivas++;
  }
  if (celula[lin-1][col+1] == '+'){
  vivas++;
  }

  if (celula[lin][col-1]  == '+'){
  vivas++;
  }
  if (celula[lin][col+1] == '+'){
  vivas++;
  }

  if (celula[lin+1][col-1] == '+'){
  vivas++;
  }
  if (celula[lin+1][col] == '+'){
  vivas++;
  }
  if (celula[lin+1][col+1] == '+'){
  vivas++;
  }

return vivas;
}

int Matriz::celulasVivas(){

  int vivas = 0, linha, coluna;

	for (linha = 0; linha < 40; linha++) {
		for (coluna = 0; coluna < 40; coluna++) {
			if (getCelula(linha, coluna) == '+'){
				vivas++;
			}
		}
	}

	return vivas;
}
