#include <iostream>
#include "gliderGun.hpp"

gliderGun::gliderGun(){

 setCelula('+',0, 24);
 setCelula('+',1, 22);
 setCelula('+',1, 24);
 setCelula('+',2, 12);
 setCelula('+',2, 13);
 setCelula('+',2, 20);
 setCelula('+',2, 21);
 setCelula('+',2, 34);
 setCelula('+',2, 35);
 setCelula('+',3, 11);
 setCelula('+',3, 15);
 setCelula('+',3, 20);
 setCelula('+',3, 21);
 setCelula('+',3, 34);
 setCelula('+',3, 35);
 setCelula('+',4, 0);
 setCelula('+',4, 1);
 setCelula('+',4, 10);
 setCelula('+',4, 16);
 setCelula('+',4, 20);
 setCelula('+',4, 21);
 setCelula('+',5, 0);
 setCelula('+',5, 1);
 setCelula('+',5, 10);
 setCelula('+',5, 14);
 setCelula('+',5, 16);
 setCelula('+',5, 17);
 setCelula('+',5, 22);
 setCelula('+',5, 24);
 setCelula('+',6, 10);
 setCelula('+',6, 16);
 setCelula('+',6, 24);
 setCelula('+',7, 11);
 setCelula('+',7, 15);
 setCelula('+',8, 12);
 setCelula('+',8, 13);

 setNome("Glider Gun");
 setRepeticoes(150);
 setTamanho(40);

}
